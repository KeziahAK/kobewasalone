#ifndef COLLISION_H
#define COLLISION_H
#include <iostream>
#include <cstring>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <../include/SDL2/SDL.h>


/* Dimensions initiales et titre de la fenetre */
static const int WINDOW_WIDTH = 1100;
static const int WINDOW_HEIGHT = 801;


//La camera
//SDL_Rect camera = { -LEVEL_WIDTH, -LEVEL_HEIGHT, LEVEL_WIDTH, LEVEL_HEIGHT };


// //The event structure
// SDL_Event event;

// SDL_Color textColor = { 255, 255, 255 };


//Création de la classe carré

struct Square {
 
    //La vitesse du point
    float vitessex;
    float vitessey;

    float SQUARE_WIDTH;
    float SQUARE_HEIGHT;
    float x,y;


    //Initialisation des variables
    Square(float w, float h, float _x, float _y);
 
    //Recupere la touche pressee et ajuste la vitesse du carre
    void handle_input(SDL_Event event);
 
    //Montre le carré sur l'ecran
    void show(Square &B, Square &A);

    void drawSquare(int R, int G, int B);

    bool check_collision( Square &B, Square &A );

    //Bouge le point
    void collision_fenetre();

};

#endif  
