#ifndef CAMERA_H
#define CAMERA_H
#include <iostream>
#include <cstring>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <../include/SDL2/SDL.h>
#include "collision.h"

//Création de la classe caméra

struct Camera{
    float xcam;
    float ycam;

    Camera(float _camx, float _camy);

    //Met la camera sur le point
    void set_camera(Square &A);
};

#endif