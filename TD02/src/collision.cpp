#include "collision.h"


Square::Square(float w, float h, float _x, float _y){
    
    //Initialize the offsets
    x = _x;
    y = _y;

    //Set the square's dimentions
    SQUARE_WIDTH=w;
    SQUARE_HEIGHT=h;
    

    //Initialize the velocity
    vitessex = 0;
    vitessey = 0;

}

void Square::drawSquare(int R, int G, int B){

    glBegin(GL_QUADS);
        glColor3f (R,G,B);
        glVertex2f(x,y);
        glVertex2f(x+SQUARE_WIDTH,y);
        glVertex2f(x+SQUARE_WIDTH,y+SQUARE_HEIGHT);
        glVertex2f(x,y+SQUARE_HEIGHT);
        
       
    glEnd();
}



//La fonction qui prend en compte la vitesse du cube en fonction de si on appuie sur une touche ou pas
void Square::handle_input(SDL_Event event)
{
        
    //Si une touche a ete pressee
    if( event.type == SDL_KEYDOWN )
    {
        //ajustement de la vitesse
        switch( event.key.keysym.sym )
        {
            //case SDLK_DOWN: vitessey -= SQUARE_HEIGHT/40 ; break;
            case SDLK_UP: vitessey=SQUARE_HEIGHT/100; break;
            case SDLK_LEFT: vitessex -= SQUARE_WIDTH/200; break;
            case SDLK_RIGHT: vitessex += SQUARE_WIDTH/200; break;
            default: break;
        }
    }
//Si une touche a ete relachee
    else if( event.type == SDL_KEYUP )
    {
        //ajustement de la vitesse
        switch( event.key.keysym.sym )
        {
            case SDLK_DOWN: vitessey = 0; break;
            case SDLK_UP: vitessey=-SQUARE_HEIGHT/100; 
            case SDLK_LEFT: vitessex = 0; break;
            case SDLK_RIGHT: vitessex = 0; break;
            default: break;
        }
    }
}

bool Square::check_collision( Square &B, Square &A)
{
    //Les cotes des rectangles
    float leftB;
    float rightB;
    float topB;
    float bottomB;

    float leftA;
    float rightA;
    float topA;
    float bottomA;
 
    //Calcul les cotes du rectangle B
    leftB = B.x;
    rightB = B.x + B.SQUARE_WIDTH;
    topB = B.y;
    bottomB = B.y + B.SQUARE_HEIGHT;

    leftA = A.x;
    rightA = A.x + A.SQUARE_WIDTH;
    topA = A.y;
    bottomA = A.y + A.SQUARE_HEIGHT;

    //Tests de collision
    if( bottomA  <= topB )
    {
        return false;
    }
 
    if( topA >= bottomB )
    {
        return false;
    }
 
    if( rightA <= leftB )
    {
        return false;
    }
 
    if( leftA >= rightB )
    {
        return false;
    }
 
    //Si conditions collision detectee
    return true;
}




//on ne va plus tester les collisions avec les bords de notre fenêtre SDL et donc de l'écran mais avec les bords du niveau.
void Square::show(Square &A, Square &B) {

     B.x += vitessex;
    //Si collision avec le mur
    if( ( check_collision( B, A )) ) {
        // collision droite
        if(B.x + B.SQUARE_WIDTH > A.x + A.SQUARE_WIDTH) {
            B.x = A.x + A.SQUARE_WIDTH+ B.SQUARE_WIDTH;
        }
        // collision gauche
        if(B.x -2*B.SQUARE_WIDTH< A.x) {
            B.x = A.x - B.SQUARE_WIDTH;
        }
    }
 
    //Bouge le carre vers le haut ou vers le bas
    B.y += vitessey;

    //Si collision avec le mur
    if( ( check_collision( B, A )) ) {
        // collision haut
        if(B.y +B.SQUARE_HEIGHT < A.y){
            B.y = A.y + 2*B.SQUARE_HEIGHT;
        }
 
        // collision bas
        if(B.y  > A.y - A.SQUARE_HEIGHT -B.SQUARE_HEIGHT) {
            B.y = A.y + A.SQUARE_HEIGHT;
        }
    }
}



void Square::collision_fenetre() {

    x += vitessex;
        //Si collision avec les cotes de l'ecran (droite ou gauche)

    if( ( x < -980  ) ) {
        //On colle le carre contre l'ecran
        x = -980 ;
    }
    if ( x> 1100) {
         //On colle le carre contre l'ecran
         x = 1100 ;
    }
   
 
    //Bouge le carre vers le haut ou vers le bas
    y += vitessey;
 
    //Si collision avec les cotes de l'ecran (haut ou bas)
    if( ( y < -300 ) ) {
        //On colle le carre contre l'ecran
        y = - 70;
        x = - 970;
    }
    // if ( y + SQUARE_HEIGHT > WINDOW_HEIGHT -SQUARE_WIDTH) {
    //     //On colle le carre contre l'ecran
    //     y =WINDOW_HEIGHT - SQUARE_HEIGHT ;
    // }
    
}