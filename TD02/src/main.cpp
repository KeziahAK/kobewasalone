#include <iostream>
#include <cstring>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <../include/SDL2/SDL.h>
#include "camera.h"
#include "collision.h"
#include "QuadTree.h"
#include "SDL_image.h"


static float aspectRatio;
static const  int BIT_PER_PIXEL = 32;

SDL_Renderer *renderer = NULL;
SDL_Texture *image1 = NULL;
SDL_Texture *image2 = NULL;

bool menu;

/* Dimensions initiales et titre de la fenetre */

static const char WINDOW_TITLE[] = "KobeWasAlone";
SDL_Surface *background =NULL;
//SDL_Surface *screen =NULL;

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE = 400.;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;


void onWindowResized(unsigned int width, unsigned int height)
{ 
    aspectRatio = width / (float) height;

    glViewport(0, 0, 2*width, 2*height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if( aspectRatio > 1) 
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2. * aspectRatio, GL_VIEW_SIZE / 2. * aspectRatio, 
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.);
    }
    else
    {
        gluOrtho2D(
        -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
        -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    }
}


void draw(){
/*     SDL_SetRenderDrawColor(renderer, 40, 255, 0, 0);
    SDL_Rect rect;
    rect.x = rect.y = 0;
    rect.w = WINDOW_WIDTH;
    rect.h = WINDOW_HEIGHT;
    SDL_RenderFillRect(renderer, &rect); */


    SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, image1, NULL, NULL);
    //if bool == true on affiche tous le bloc (entre autre le jeu) sinon si bool= dalse s active pas
    // second bloc = du debut. qd le bool est false le bloc du debut s active else rien se passe
    //on cree une fonction menu : toutes les fonctions pr le menu 
    //on cree une fonction game : toutes les fonctions de kez
    //Square W1 (150, 20,-1000,-70);
    //W1.drawSquare(1,0,1);


    
}

void draw_image(){



    SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, image2, NULL, NULL);


    
}


int main(int argc, char** argv) 
{
    /* Initialisation de la SDL */

    if(SDL_Init(SDL_INIT_VIDEO) < 0) 
    {
        const char* error = SDL_GetError();
        fprintf(
            stderr, 
            "Erreur lors de l'intialisation de la SDL : %s\n", error);

        SDL_Quit();
        return EXIT_FAILURE;
    }
	
    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */

    SDL_Window* window;
    {
        window = SDL_CreateWindow(
        WINDOW_TITLE,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE|SDL_WINDOW_ALLOW_HIGHDPI );

        if(NULL == window) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation de la fenetre : %s\n", error);

            SDL_Quit();
            return EXIT_FAILURE;
        }
    }

    
    SDL_GLContext context;
    {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

            SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        context = SDL_GL_CreateContext(window);
    
        if(NULL == context) 
        {
            const char* error = SDL_GetError();
            fprintf(
                stderr,
                "Erreur lors de la creation du contexte OpenGL : %s\n", error);

            SDL_DestroyWindow(window);
            SDL_Quit();
            return EXIT_FAILURE;
        }
    } 


    // renderer = SDL_CreateRenderer(window, -1, 0);
    // if(renderer == NULL) {
    //     fprintf(stderr, "Impossible de créer le renderer. Fin du programme.\n");
    //     return EXIT_FAILURE;
    // }   
  
    // Création de la texture pour l'affichage dans la fenêtre
     IMG_Init(IMG_INIT_PNG);
     image1 = IMG_LoadTexture(renderer, "./src/start_screen.png");
	 //SDL_FreeSurface(framebuffer); 
     image2 = IMG_LoadTexture(renderer, "./src/youwon.png");

    
    menu = true;

    onWindowResized(WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Boucle principale */
    int loop = 1;
	Square square(10,10,- 970 ,- 70);
    Square square2 (20,10,- 960 ,- 50);
    //Square square3 (10,20,- WINDOW_WIDTH +10 ,- WINDOW_HEIGHT +2*30);
    
    Camera camera(0,0);


    //Map
    
    Square mur2 (70, 20,0,0 );
    Square mur3 (20, 20,80,-10 );
    Square mur4 (70, 20,120,20 );
    Square mur5 (10, 50,130,20 );
    Square mur6 (70, 20,-100,-40 );
    Square mur7 (70, 20,-200,-100 );
    Square mur8 (70, 20,-300,-80 );
    Square mur9 (50, 20,-400,-60 );
    Square mur10 (150, 20,400,15);
    Square mur11 (10, 50,450,15);
    Square mur12 (10, 20,500,15);
    Square mur13 (50, 20,310,0);
    Square mur14 (60, 20,220,0);
    Square mur15 (150, 20,600,-20);
    Square mur16 (50,30,700,-20);
    Square mur17 (150, 20,800,-60);
    Square mur18 (50, 20,900,-10);
    Square mur19 (150, 20,1000,-60);
    Square mur20 (150, 20,-600,-40);
    Square mur21 (150, 20,-800,-60);
    Square mur22 (150, 20,-1000,-70);
    Square mur23 (30, 50,-900,-70);



    

    

    //Start
    Square S0 (5,10,-WINDOW_WIDTH -100,-55);
    Square S1 (10,5,-WINDOW_WIDTH -100,-60);
    Square S2 (5,10,-WINDOW_WIDTH -90,-60);
    Square S3 (10,5,-WINDOW_WIDTH-90, -50);
    Square S4 (5,15,-WINDOW_WIDTH-80, -60);

    Square T0 (25,5,-WINDOW_WIDTH-100,-35);
    Square T1 (5,15,-WINDOW_WIDTH -100,-40);

    Square A0 (25,5,-WINDOW_WIDTH-100,-20);
    Square A1 (25,5,-WINDOW_WIDTH-100,-10);
    Square A2 (5,10,-WINDOW_WIDTH -100,-20);
    Square A3 (5,10,-WINDOW_WIDTH -90,-20);

    Square R0 (25,5,-WINDOW_WIDTH-100,0);
    Square R1 (15,5,-WINDOW_WIDTH-100,10);
    Square R2 (5,10,-WINDOW_WIDTH -100,0);
    Square R3 (5,10,-WINDOW_WIDTH -90,0);   
    Square R4 (15,5,-WINDOW_WIDTH-90,8);   
    
    Square _T0 (25,5,-WINDOW_WIDTH-100,25);
    Square _T1 (5,15,-WINDOW_WIDTH -100,20);
    
    

    while(loop) 
    {   

        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();
        SDL_Event event;
        bool j1=true;
        const Uint8 *keystates = SDL_GetKeyboardState(NULL);
        while(SDL_PollEvent(&event)){
            if(keystates[SDL_SCANCODE_RETURN] ) {menu = false;}
            if (keystates[SDL_SCANCODE_A]) {j1= false;}

           
        }
        
        if(menu==false &&j1==true ){
            
            /* Placer ici le code de dessin */
            glClear(GL_COLOR_BUFFER_BIT);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            camera.set_camera(square); 
            
            mur2.drawSquare(1,0,1);
            mur3.drawSquare(1,0,1);
            mur4.drawSquare(1,0,1);
            mur5.drawSquare(1,0,1);
            mur6.drawSquare(1,0,1);
            mur7.drawSquare(1,0,1);
            mur8.drawSquare(1,0,1);
            mur9.drawSquare(1,0,1);
            mur10.drawSquare(1,0,1);
            mur11.drawSquare(1,0,1);
            mur12.drawSquare(1,0,1);
            mur13.drawSquare(1,0,1);
            mur14.drawSquare(1,0,1);
            mur15.drawSquare(1,0,1);
            mur16.drawSquare(1,0,1);
            mur17.drawSquare(1,0,1);
            mur18.drawSquare(1,0,1);
            mur19.drawSquare(1,0,1);
            mur20.drawSquare(1,0,1);
            mur21.drawSquare(1,0,1);
            mur22.drawSquare(1,0,1);
            //mur23.drawSquare(1/2,0,1);
            

            //Start
            S0.drawSquare(0,1,1/2);
            S1.drawSquare(0,1,1/2);
            S2.drawSquare(0,1,1/2);
            S3.drawSquare(0,1,1/2);
            S4.drawSquare(0,1,1/2);

            T0.drawSquare(0,1,1/2);
            T1.drawSquare(0,1,1/2);

            A0.drawSquare(0,1,1/2);
            A1.drawSquare(0,1,1/2);
            A2.drawSquare(0,1,1/2);
            A3.drawSquare(0,1,1/2);

            R0.drawSquare(0,1,1/2);
            R1.drawSquare(0,1,1/2);
            R2.drawSquare(0,1,1/2);
            R3.drawSquare(0,1,1/2);
            R4.drawSquare(0,1,1/2);
    
            _T0.drawSquare(0,1,1/2);
            _T1.drawSquare(0,1,1/2);

            square.collision_fenetre();
            square2.collision_fenetre();
            
            square.show(mur2,square);
            square.show(mur3,square);
            square.show(mur4,square);
            square.show(mur5,square);
            square.show(mur6,square);
            square.show(mur7,square);
            square.show(mur8,square);
            square.show(mur9,square);
            square.show(mur10,square);
            square.show(mur11,square);
            square.show(mur12,square);
            square.show(mur13,square);
            square.show(mur14,square);
            square.show(mur15,square);
            square.show(mur16,square);
            square.show(mur17,square);
            square.show(mur18,square);
            square.show(mur19,square);
            square.show(mur20,square);
            square.show(mur21,square);
            square.show(mur22,square);
            //square.show(mur23,square);
            //square.show(square2,square);




            //premier carré qui bouge
            square.x += square.vitessex;
            square.drawSquare(0,1,1);
            square.y += square.vitessey;
            square.drawSquare(0,1,1);
            

            // square2.x += square2.vitessex;
            // square2.drawSquare(1,1,1);
            // square2.y += square2.vitessey;
            // square2.drawSquare(1,1,1);
            
 
            
            
            
            
            
            /* Echange du front et du back buffer : mise a jour de la fenetre */
            SDL_GL_SwapWindow(window);
            
            /* Boucle traitant les evenements */
            SDL_Event e;
            while(SDL_PollEvent(&e)) 
            {
                
                // while (SDL_PollEvent(&ev)){
                //     if( e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_a){
                //         square.handle_input(e); 
                //     }
                //     else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q){
                //         square2.handle_input(e);
                //     }
                // }

                

                //if (j1==true){
                    square.handle_input(e);
                //}

                //if(j2==true){
                    square2.handle_input(e);
                //}
                


                /* L'utilisateur ferme la fenetre : */
                if(e.type == SDL_QUIT) 
                {
                    loop = 0;
                    break;
                }
            
                if(	e.type == SDL_KEYDOWN 
                    && (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
                {
                    loop = 0; 
                    break;
                }
            
                
                //SDL_Delay(10);
                
                
                
                switch(e.type) 
                {
                    case SDL_WINDOWEVENT:
                        switch (e.window.event) 
                        {
                            /* Redimensionnement fenetre */
                            case SDL_WINDOWEVENT_RESIZED:
                                onWindowResized(e.window.data1, e.window.data2);                
                                break;

                            default:
                                break; 
                        }
                        break;

                    /* Clic souris */
                    case SDL_MOUSEBUTTONUP:
                        printf("clic en (%d, %d)\n", e.button.x, e.button.y);
                        break;
                    
                    /* Touche clavier */
                    case SDL_KEYDOWN:
                        printf("touche pressee (code = %d)\n", e.key.keysym.sym);
                        break;
                        
                    default:
                        break;
                }
            }        
        }
        else if(menu==true){
            //draw_image();
            draw();
        }
        else if (j1==false){
                draw_image();
        }

    


        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS) 
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    /* Liberation des ressources associees a la SDL */ 

    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return EXIT_SUCCESS;
}




