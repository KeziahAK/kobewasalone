#ifndef QUADTREE_H
#define QUADTREE_H
#include <iostream>
#include <cmath>
using namespace std;

//structure de points de coordonnées x et y
struct XY
{
	int x;
	int y;

	//initialisation
	XY(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	XY()
	{
		x = 0;
		y = 0;
	}
};


//objects que nous voulons stocker dans le quadTree
struct Noeud
{
	XY pos;//position de l'objet
	int data;//Stockage de l'objet

	//initialisation
	Noeud(XY _pos, int _data)
	{
		pos = _pos;
		data = _data;//servie pour stocker le noeud
	}
	Noeud()
	{
		data = 0;
	}
};

//la classe ci-dessous représente à la fois un quadtree et son noeud parent
class QuadTree
{
	//Limites du noeud
	XY topLeft;
	XY botRight;

	//Comme ca pas besoin de tableau pour savoir si on dépasse ou pas la capacité, on va juste se baser sur les limites

    //détails du noeud
	Noeud *n;

	//Les 4 enfants du QuadTree de base (quand on sépare chaquecase en 4)
	QuadTree *topLeftTree;
	QuadTree *topRightTree;
	QuadTree *botLeftTree;
	QuadTree *botRightTree;

public:

//initialisation
	QuadTree()
	{
		topLeft = XY(0, 0);//Le premier noeud se plasse à l'origine du repère
		botRight = XY(0, 0);//On initialise le premier noeud
		n = NULL;
		topLeftTree = NULL;
		topRightTree = NULL;
		botLeftTree = NULL;
		botRightTree = NULL;
	}
	QuadTree(XY topL, XY botR)
	{
		n = NULL;
		topLeftTree = NULL;
		topRightTree = NULL;
		botLeftTree = NULL;
		botRightTree = NULL;
		topLeft = topL;
		botRight = botR;
	}

	//Méthode de séparation
	void insert(Noeud*);//insérer un noeud dans le quadTree et subdiviser le noeud
    //void subdivise (); //création de 4 enfants permettant de diviser le quadrant en quatre quadrants de dimensions égales (pas utilisée car on le fait dans insertion)
	Noeud* search(XY);// Trouver tous les XYs apparaissant à l'intérieur d'une «zone de recherche» et les stocker dans le quadtree?
	bool limites(XY);//défini les limites du quadTree
};

#endif
