#include "QuadTree.h"
#include <iostream>
#include <cmath>
using namespace std;

//Les fonctions d'insertion sont utilisées pour insérer un nœud dans un Quad Tree existant. 
        //Cette fonction vérifie d'abord si le nœud donné se trouve dans les limites du quad courant. 
        //Si ce n'est pas le cas, alors nous cessons immédiatement l'insertion. 
        //S'il se trouve à l'intérieur des limites, nous sélectionnons l'enfant approprié pour contenir ce nœud en fonction de son emplacement.
        
        //On se base encore sur les limites donc pas besoin de tableau
       
void QuadTree::insert(Noeud *noeud)
{
	 //Tout interrompre si il n'y a pas de noeud parent
	if (noeud == NULL)
		return;

	//Tout interrompre si la zone de recherche n'intersecte pas le quadrant
        //En gros si dans la zone on a pas d'objet on a aucun objet à mettre dans le quadTree
	if (!limites(noeud->pos))
		return;

	//Vérifier les objets du quadrant
    //On se trouve dans la dernière subdivision du quadTree,
    //on ne peut donc pas la diviser plus qu'elle ne l'est déjà. 
	if (abs(topLeft.x - botRight.x) <= 1 && abs(topLeft.y - botRight.y) <= 1)
	{
		if (n == NULL)
			n = noeud;
		return;
	}
	//Si le noeud se trouve à l'intérieur des limites, nous sélectionnons l'enfant approprié pour contenir ce nœud en fonction de son emplacement.
	if ((topLeft.x + botRight.x) / 2 >= noeud->pos.x)//On se trouve à gauche
	{
		// On se trouve dans le coin en haut à gauche
		if ((topLeft.y + botRight.y) / 2 >= noeud->pos.y)
		{
			if (topLeftTree == NULL)
				topLeftTree = new QuadTree( XY(topLeft.x, topLeft.y),XY((topLeft.x + botRight.x) / 2,(topLeft.y + botRight.y) / 2));
			topLeftTree->insert(noeud);//On insert le nouveau noeud dans le quad
		}

		 //On refait la même choqe mais pour chaque région des cases subdivisées
        // On se trouve dans le coin en haut à bas à gauche
		else
		{
			if (botLeftTree == NULL)
				botLeftTree = new QuadTree(XY(topLeft.x,(topLeft.y + botRight.y) / 2),XY((topLeft.x + botRight.x) / 2,botRight.y));
			botLeftTree->insert(noeud);
		}
	}
	else//On se trouve à droite
        // On se trouve dans le coin en haut à droite
	{
		// Indicates topRightTree
		if ((topLeft.y + botRight.y) / 2 >= noeud->pos.y)
		{
			if (topRightTree == NULL)
				topRightTree = new QuadTree(XY((topLeft.x + botRight.x) / 2,topLeft.y),XY(botRight.x,(topLeft.y + botRight.y) / 2));
			topRightTree->insert(noeud);
		}

		//On se trouve dans le coin en bas à droite
		else
		{
			if (botRightTree == NULL)botRightTree = new QuadTree(XY((topLeft.x + botRight.x) / 2,(topLeft.y + botRight.y) / 2),XY(botRight.x, botRight.y));
			botRightTree->insert(noeud);
		}
	}
}

//P est la position du joueur et La fonction de recherche est utilisée pour localiser un object appelé noeud dans le quad donné. 
//Il peut également être modifié pour renvoyer le nœud le plus proche du XY donné.
Noeud* QuadTree::search(XY p)
{
	//ignorer les objets qui n'appartiennent pas à ce quadTree
	if (!limites(p))
		return NULL;//l'objet ne doit pas être ajouté

	//On se trouve dans la dernière subdivision du quadTree,
    //on ne peut donc pas la diviser plus qu'elle ne l'est déjà. 
    //Si le noeud n'est pas nul on retourn donc simplement le dernier noeud.
	if (n != NULL)
		return n;

	//On se balade dans chaque quad et on cherche les noeuds 
	if ((topLeft.x + botRight.x) / 2 >= p.x)//On se trouve à gauche
	{
		//On se trouve dans le coin en haut à gauche
		if ((topLeft.y + botRight.y) / 2 >= p.y)
		{
			if (topLeftTree == NULL)
				return NULL;//si on trouve rien
			return topLeftTree->search(p);//sinon on relance la rechercche
		}

		//On se trouve dans le coin en bas à gauche
		else
		{
			if (botLeftTree == NULL)
				return NULL;
			return botLeftTree->search(p);
		}
	}
	else //On refait la même chose mais de l'autre côté du quadTree
	{
		//On se trouve à droite 
		if ((topLeft.y + botRight.y) / 2 >= p.y)//On se trouve dans le coin en haut à droite
		{
			if (topRightTree == NULL)
				return NULL;
			return topRightTree->search(p);
		}

		//On se trouve dans le coin en bas à droite 
		else
		{
			if (botRightTree == NULL)
				return NULL;
			return botRightTree->search(p);
		}
	}
};

//vérification pour voir si le quadtree contient le XY
bool QuadTree::limites(XY p)
{
	return (p.x >= topLeft.x && p.x <= botRight.x && p.y >= topLeft.y && p.y <= botRight.y);
}

//Pour résumer : La fonction inserte elle compare la position de l'object et celle du quad, 
//si les positions concordent elle ajoute l'object au quad sinon elle cesse l'insertion.
//La fonction search, elle, elle se place dans le quad contenant la position du joueur et elle cherche tous les noeuds(=objects) proche du joueur.
//Donc en gros la fonction insertion elle fabrique le quadtree et la fonction search elle aide à l'ocaliser les rectangles à proximiter du joueur.